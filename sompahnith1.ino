const int R = 2;    
const int G = 4; 
const int B = 3;  

void setup() {
 
  Serial.begin(9600);

 
  pinMode(R, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(B, OUTPUT);
  
  
  digitalWrite(R, LOW);
  digitalWrite(G, LOW);
  digitalWrite(B, LOW);
}

void loop() {

  if (Serial.available() > 0) {
   
    String input = Serial.readStringUntil('\n'); 
  
    input.trim();

   
    digitalWrite(R, LOW);
    digitalWrite(G, LOW);
    digitalWrite(B, LOW);
    
   
    if (input == "r") { 
      digitalWrite(R, HIGH);
    } else if (input == "g") {
      digitalWrite(G, HIGH);
    } else if (input == "b") { 
      digitalWrite(B, HIGH);
    } else if (input == "on") { 
      digitalWrite(R, HIGH);
      digitalWrite(G, HIGH);
      digitalWrite(B, HIGH);
    } else if (input == "off") {
      
    } else {
      
    }
  }
}